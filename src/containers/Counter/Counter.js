import React, {Component} from "react";
import "./Counter.css";

import {connect} from "react-redux";
import {changeCounter, fetchCounter} from "../../store/actions";
import Spinner from "../../components/UI/Spinner/Spinner";

class Counter extends Component {
    componentDidMount() {
        this.props.fetchCounter();

    };


    render() {
        let spinner;
        if (this.props.error)
            return <div><h1>ERROR!!!!!!</h1></div>;
        if (this.props.req)
            spinner = <Spinner/>
        else
            spinner = <h1>{this.props.ctr}</h1>;
        return (<div className="Counter">
            {spinner}
            <button onClick={this.props.increaseCounter}>Increment</button>
            <button onClick={this.props.decreaseCounter}>Decrement</button>
            <button onClick={this.props.addCounter}>Increment by 5</button>
            <button onClick={this.props.subtractCounter}>Descrease by 5</button>
        </div>)
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter,
        req: state.request,
        error: state.error
    }
};

const mapDispatchToProps = dispatch => {
    return {
        increaseCounter: () => dispatch(changeCounter(1)),
        decreaseCounter: () => dispatch(changeCounter(-1)),
        addCounter: () => dispatch(changeCounter(5)),
        subtractCounter: () => dispatch(changeCounter(-5)),
        fetchCounter: () => dispatch(fetchCounter()),

    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Counter);