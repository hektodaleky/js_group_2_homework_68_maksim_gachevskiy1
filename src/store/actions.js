import axios from "../axios-counter";
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';



export const changeCounter = amount => {
    return putCounter(amount)
};



export const fetchCounterRequest = () => {
    return {type: FETCH_COUNTER_REQUEST}
};
export const fetchCounterSuccess = (counter) => {
    return {type: FETCH_COUNTER_SUCCESS, counter}
};

export const fetchCounterError = () => {
    return {type: FETCH_COUNTER_ERROR}
};

export const fetchCounter = () => {

    return (dispatch, getState) => {
        dispatch(fetchCounterRequest());
        axios.get('/counter.json').then(response => {
            dispatch(fetchCounterSuccess(response.data));

        }, error => {
            dispatch(fetchCounterError())
        })
    }
};


export const putCounter = (n) => {

    return (dispatch, getState) => {
        console.log(getState());
        dispatch(fetchCounterRequest());
        axios.put('/counter.json',getState().counter+n).then(response => {
            dispatch(fetchCounterSuccess(response.data));

        }, error => {
            dispatch(fetchCounterError())
        })
    }
};