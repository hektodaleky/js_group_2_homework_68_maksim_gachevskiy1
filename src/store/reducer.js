import * as actionTypes from "./actions";
const initialState = {
        counter: 0,
        request: false,
        error: false
    }
;
const reducer = (state = initialState, action) => {

    if (action.type === actionTypes.FETCH_COUNTER_REQUEST) {
        return {...state, request: true};

    }
    if (action.type === actionTypes.FETCH_COUNTER_SUCCESS) {
        return {...state, counter: action.counter, request: false, error: false};

    }
    if (action.type === actionTypes.FETCH_COUNTER_ERROR) {
        console.log("ERRROORRR");
        return {...state, request: false, error: true};

    }
    return state;
};
export default reducer;